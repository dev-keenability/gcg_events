PGDMP                     	    u            dddusbdeohe67a    9.6.2    9.6.0 B    %           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            &           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            '           1262    8039873    dddusbdeohe67a    DATABASE     �   CREATE DATABASE "dddusbdeohe67a" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
     DROP DATABASE "dddusbdeohe67a";
             niqjazdxhjbefa    false                        2615    11500241    public    SCHEMA        CREATE SCHEMA "public";
    DROP SCHEMA "public";
             niqjazdxhjbefa    false            (           0    0    SCHEMA "public"    COMMENT     8   COMMENT ON SCHEMA "public" IS 'standard public schema';
                  niqjazdxhjbefa    false    7                        3079    13277    plpgsql 	   EXTENSION     C   CREATE EXTENSION IF NOT EXISTS "plpgsql" WITH SCHEMA "pg_catalog";
    DROP EXTENSION "plpgsql";
                  false            )           0    0    EXTENSION "plpgsql"    COMMENT     B   COMMENT ON EXTENSION "plpgsql" IS 'PL/pgSQL procedural language';
                       false    1            �            1259    11500244 
   applicants    TABLE       CREATE TABLE "applicants" (
    "id" integer NOT NULL,
    "full_name" character varying,
    "email" character varying,
    "phone_number" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 "   DROP TABLE "public"."applicants";
       public         niqjazdxhjbefa    false    7            �            1259    11500250    applicants_id_seq    SEQUENCE     u   CREATE SEQUENCE "applicants_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE "public"."applicants_id_seq";
       public       niqjazdxhjbefa    false    185    7            *           0    0    applicants_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE "applicants_id_seq" OWNED BY "applicants"."id";
            public       niqjazdxhjbefa    false    186            �            1259    11500252    ar_internal_metadata    TABLE     �   CREATE TABLE "ar_internal_metadata" (
    "key" character varying NOT NULL,
    "value" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 ,   DROP TABLE "public"."ar_internal_metadata";
       public         niqjazdxhjbefa    false    7            �            1259    11500258    blogs    TABLE     �  CREATE TABLE "blogs" (
    "id" integer NOT NULL,
    "title" character varying,
    "content" "text",
    "content_index" "text",
    "title_for_slug" character varying,
    "main_image" character varying,
    "pubdate" timestamp without time zone,
    "meta_description" character varying,
    "meta_keywords" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
    DROP TABLE "public"."blogs";
       public         niqjazdxhjbefa    false    7            �            1259    11500264    blogs_id_seq    SEQUENCE     p   CREATE SEQUENCE "blogs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE "public"."blogs_id_seq";
       public       niqjazdxhjbefa    false    188    7            +           0    0    blogs_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE "blogs_id_seq" OWNED BY "blogs"."id";
            public       niqjazdxhjbefa    false    189            �            1259    11500266    contacts    TABLE       CREATE TABLE "contacts" (
    "id" integer NOT NULL,
    "name" character varying,
    "email" character varying,
    "phone_number" character varying,
    "message" "text",
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
     DROP TABLE "public"."contacts";
       public         niqjazdxhjbefa    false    7            �            1259    11500272    contacts_id_seq    SEQUENCE     s   CREATE SEQUENCE "contacts_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE "public"."contacts_id_seq";
       public       niqjazdxhjbefa    false    190    7            ,           0    0    contacts_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE "contacts_id_seq" OWNED BY "contacts"."id";
            public       niqjazdxhjbefa    false    191            �            1259    11500274    images    TABLE     �  CREATE TABLE "images" (
    "id" integer NOT NULL,
    "imageable_id" integer,
    "imageable_type" character varying,
    "name" character varying,
    "pic_file_name" character varying,
    "pic_content_type" character varying,
    "pic_file_size" integer,
    "pic_updated_at" timestamp without time zone,
    "primary" boolean,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
    DROP TABLE "public"."images";
       public         niqjazdxhjbefa    false    7            �            1259    11500280    images_id_seq    SEQUENCE     q   CREATE SEQUENCE "images_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE "public"."images_id_seq";
       public       niqjazdxhjbefa    false    192    7            -           0    0    images_id_seq    SEQUENCE OWNED BY     7   ALTER SEQUENCE "images_id_seq" OWNED BY "images"."id";
            public       niqjazdxhjbefa    false    193            �            1259    11500282 	   locations    TABLE     =  CREATE TABLE "locations" (
    "id" integer NOT NULL,
    "name" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "title_for_slug" character varying,
    "description" "text",
    "phone" character varying,
    "fax" character varying,
    "specific_location" character varying,
    "iframe" "text",
    "address" character varying,
    "latitude" double precision,
    "longitude" double precision,
    "title_for_maps" character varying,
    "general_contact_email" character varying
);
 !   DROP TABLE "public"."locations";
       public         niqjazdxhjbefa    false    7            �            1259    11500288    locations_id_seq    SEQUENCE     t   CREATE SEQUENCE "locations_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE "public"."locations_id_seq";
       public       niqjazdxhjbefa    false    194    7            .           0    0    locations_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE "locations_id_seq" OWNED BY "locations"."id";
            public       niqjazdxhjbefa    false    195            �            1259    11500290    profiles    TABLE     |  CREATE TABLE "profiles" (
    "id" integer NOT NULL,
    "full_name" character varying,
    "title" character varying,
    "telephone" character varying,
    "mobile" character varying,
    "email" character varying,
    "location_id" integer,
    "position" integer,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
     DROP TABLE "public"."profiles";
       public         niqjazdxhjbefa    false    7            �            1259    11500296    profiles_id_seq    SEQUENCE     s   CREATE SEQUENCE "profiles_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE "public"."profiles_id_seq";
       public       niqjazdxhjbefa    false    7    196            /           0    0    profiles_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE "profiles_id_seq" OWNED BY "profiles"."id";
            public       niqjazdxhjbefa    false    197            �            1259    11500299    schema_migrations    TABLE     O   CREATE TABLE "schema_migrations" (
    "version" character varying NOT NULL
);
 )   DROP TABLE "public"."schema_migrations";
       public         niqjazdxhjbefa    false    7            �            1259    11500305    users    TABLE     �  CREATE TABLE "users" (
    "id" integer NOT NULL,
    "email" character varying DEFAULT ''::character varying NOT NULL,
    "encrypted_password" character varying DEFAULT ''::character varying NOT NULL,
    "reset_password_token" character varying,
    "reset_password_sent_at" timestamp without time zone,
    "remember_created_at" timestamp without time zone,
    "sign_in_count" integer DEFAULT 0 NOT NULL,
    "current_sign_in_at" timestamp without time zone,
    "last_sign_in_at" timestamp without time zone,
    "current_sign_in_ip" "inet",
    "last_sign_in_ip" "inet",
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "admin" boolean
);
    DROP TABLE "public"."users";
       public         niqjazdxhjbefa    false    7            �            1259    11500314    users_id_seq    SEQUENCE     p   CREATE SEQUENCE "users_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE "public"."users_id_seq";
       public       niqjazdxhjbefa    false    199    7            0           0    0    users_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE "users_id_seq" OWNED BY "users"."id";
            public       niqjazdxhjbefa    false    200            ~           2604    11500316    applicants id    DEFAULT     j   ALTER TABLE ONLY "applicants" ALTER COLUMN "id" SET DEFAULT "nextval"('"applicants_id_seq"'::"regclass");
 B   ALTER TABLE "public"."applicants" ALTER COLUMN "id" DROP DEFAULT;
       public       niqjazdxhjbefa    false    186    185                       2604    11500317    blogs id    DEFAULT     `   ALTER TABLE ONLY "blogs" ALTER COLUMN "id" SET DEFAULT "nextval"('"blogs_id_seq"'::"regclass");
 =   ALTER TABLE "public"."blogs" ALTER COLUMN "id" DROP DEFAULT;
       public       niqjazdxhjbefa    false    189    188            �           2604    11500318    contacts id    DEFAULT     f   ALTER TABLE ONLY "contacts" ALTER COLUMN "id" SET DEFAULT "nextval"('"contacts_id_seq"'::"regclass");
 @   ALTER TABLE "public"."contacts" ALTER COLUMN "id" DROP DEFAULT;
       public       niqjazdxhjbefa    false    191    190            �           2604    11500319 	   images id    DEFAULT     b   ALTER TABLE ONLY "images" ALTER COLUMN "id" SET DEFAULT "nextval"('"images_id_seq"'::"regclass");
 >   ALTER TABLE "public"."images" ALTER COLUMN "id" DROP DEFAULT;
       public       niqjazdxhjbefa    false    193    192            �           2604    11500320    locations id    DEFAULT     h   ALTER TABLE ONLY "locations" ALTER COLUMN "id" SET DEFAULT "nextval"('"locations_id_seq"'::"regclass");
 A   ALTER TABLE "public"."locations" ALTER COLUMN "id" DROP DEFAULT;
       public       niqjazdxhjbefa    false    195    194            �           2604    11500321    profiles id    DEFAULT     f   ALTER TABLE ONLY "profiles" ALTER COLUMN "id" SET DEFAULT "nextval"('"profiles_id_seq"'::"regclass");
 @   ALTER TABLE "public"."profiles" ALTER COLUMN "id" DROP DEFAULT;
       public       niqjazdxhjbefa    false    197    196            �           2604    11500322    users id    DEFAULT     `   ALTER TABLE ONLY "users" ALTER COLUMN "id" SET DEFAULT "nextval"('"users_id_seq"'::"regclass");
 =   ALTER TABLE "public"."users" ALTER COLUMN "id" DROP DEFAULT;
       public       niqjazdxhjbefa    false    200    199                      0    11500244 
   applicants 
   TABLE DATA               g   COPY "applicants" ("id", "full_name", "email", "phone_number", "created_at", "updated_at") FROM stdin;
    public       niqjazdxhjbefa    false    185            1           0    0    applicants_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('"applicants_id_seq"', 2, true);
            public       niqjazdxhjbefa    false    186                      0    11500252    ar_internal_metadata 
   TABLE DATA               U   COPY "ar_internal_metadata" ("key", "value", "created_at", "updated_at") FROM stdin;
    public       niqjazdxhjbefa    false    187                      0    11500258    blogs 
   TABLE DATA               �   COPY "blogs" ("id", "title", "content", "content_index", "title_for_slug", "main_image", "pubdate", "meta_description", "meta_keywords", "created_at", "updated_at") FROM stdin;
    public       niqjazdxhjbefa    false    188            2           0    0    blogs_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('"blogs_id_seq"', 3, true);
            public       niqjazdxhjbefa    false    189                      0    11500266    contacts 
   TABLE DATA               k   COPY "contacts" ("id", "name", "email", "phone_number", "message", "created_at", "updated_at") FROM stdin;
    public       niqjazdxhjbefa    false    190            3           0    0    contacts_id_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('"contacts_id_seq"', 3, true);
            public       niqjazdxhjbefa    false    191                      0    11500274    images 
   TABLE DATA               �   COPY "images" ("id", "imageable_id", "imageable_type", "name", "pic_file_name", "pic_content_type", "pic_file_size", "pic_updated_at", "primary", "created_at", "updated_at") FROM stdin;
    public       niqjazdxhjbefa    false    192            4           0    0    images_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('"images_id_seq"', 94, true);
            public       niqjazdxhjbefa    false    193                      0    11500282 	   locations 
   TABLE DATA               �   COPY "locations" ("id", "name", "created_at", "updated_at", "title_for_slug", "description", "phone", "fax", "specific_location", "iframe", "address", "latitude", "longitude", "title_for_maps", "general_contact_email") FROM stdin;
    public       niqjazdxhjbefa    false    194            5           0    0    locations_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('"locations_id_seq"', 13, true);
            public       niqjazdxhjbefa    false    195                      0    11500290    profiles 
   TABLE DATA               �   COPY "profiles" ("id", "full_name", "title", "telephone", "mobile", "email", "location_id", "position", "created_at", "updated_at") FROM stdin;
    public       niqjazdxhjbefa    false    196            6           0    0    profiles_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('"profiles_id_seq"', 50, true);
            public       niqjazdxhjbefa    false    197                       0    11500299    schema_migrations 
   TABLE DATA               1   COPY "schema_migrations" ("version") FROM stdin;
    public       niqjazdxhjbefa    false    198            !          0    11500305    users 
   TABLE DATA                 COPY "users" ("id", "email", "encrypted_password", "reset_password_token", "reset_password_sent_at", "remember_created_at", "sign_in_count", "current_sign_in_at", "last_sign_in_at", "current_sign_in_ip", "last_sign_in_ip", "created_at", "updated_at", "admin") FROM stdin;
    public       niqjazdxhjbefa    false    199            7           0    0    users_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('"users_id_seq"', 1, true);
            public       niqjazdxhjbefa    false    200            �           2606    11500325    applicants applicants_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY "applicants"
    ADD CONSTRAINT "applicants_pkey" PRIMARY KEY ("id");
 J   ALTER TABLE ONLY "public"."applicants" DROP CONSTRAINT "applicants_pkey";
       public         niqjazdxhjbefa    false    185    185            �           2606    11500327 .   ar_internal_metadata ar_internal_metadata_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY "ar_internal_metadata"
    ADD CONSTRAINT "ar_internal_metadata_pkey" PRIMARY KEY ("key");
 ^   ALTER TABLE ONLY "public"."ar_internal_metadata" DROP CONSTRAINT "ar_internal_metadata_pkey";
       public         niqjazdxhjbefa    false    187    187            �           2606    11500329    blogs blogs_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY "blogs"
    ADD CONSTRAINT "blogs_pkey" PRIMARY KEY ("id");
 @   ALTER TABLE ONLY "public"."blogs" DROP CONSTRAINT "blogs_pkey";
       public         niqjazdxhjbefa    false    188    188            �           2606    11500331    contacts contacts_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY "contacts"
    ADD CONSTRAINT "contacts_pkey" PRIMARY KEY ("id");
 F   ALTER TABLE ONLY "public"."contacts" DROP CONSTRAINT "contacts_pkey";
       public         niqjazdxhjbefa    false    190    190            �           2606    11500333    images images_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY "images"
    ADD CONSTRAINT "images_pkey" PRIMARY KEY ("id");
 B   ALTER TABLE ONLY "public"."images" DROP CONSTRAINT "images_pkey";
       public         niqjazdxhjbefa    false    192    192            �           2606    11500335    locations locations_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY "locations"
    ADD CONSTRAINT "locations_pkey" PRIMARY KEY ("id");
 H   ALTER TABLE ONLY "public"."locations" DROP CONSTRAINT "locations_pkey";
       public         niqjazdxhjbefa    false    194    194            �           2606    11500337    profiles profiles_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY "profiles"
    ADD CONSTRAINT "profiles_pkey" PRIMARY KEY ("id");
 F   ALTER TABLE ONLY "public"."profiles" DROP CONSTRAINT "profiles_pkey";
       public         niqjazdxhjbefa    false    196    196            �           2606    11500339 (   schema_migrations schema_migrations_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY "schema_migrations"
    ADD CONSTRAINT "schema_migrations_pkey" PRIMARY KEY ("version");
 X   ALTER TABLE ONLY "public"."schema_migrations" DROP CONSTRAINT "schema_migrations_pkey";
       public         niqjazdxhjbefa    false    198    198            �           2606    11500341    users users_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY "users"
    ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
 @   ALTER TABLE ONLY "public"."users" DROP CONSTRAINT "users_pkey";
       public         niqjazdxhjbefa    false    199    199            �           1259    11500342 /   index_images_on_imageable_id_and_imageable_type    INDEX     }   CREATE INDEX "index_images_on_imageable_id_and_imageable_type" ON "images" USING "btree" ("imageable_id", "imageable_type");
 G   DROP INDEX "public"."index_images_on_imageable_id_and_imageable_type";
       public         niqjazdxhjbefa    false    192    192            �           1259    11500343 !   index_locations_on_title_for_slug    INDEX     i   CREATE UNIQUE INDEX "index_locations_on_title_for_slug" ON "locations" USING "btree" ("title_for_slug");
 9   DROP INDEX "public"."index_locations_on_title_for_slug";
       public         niqjazdxhjbefa    false    194            �           1259    11500344    index_users_on_email    INDEX     O   CREATE UNIQUE INDEX "index_users_on_email" ON "users" USING "btree" ("email");
 ,   DROP INDEX "public"."index_users_on_email";
       public         niqjazdxhjbefa    false    199            �           1259    11500345 #   index_users_on_reset_password_token    INDEX     m   CREATE UNIQUE INDEX "index_users_on_reset_password_token" ON "users" USING "btree" ("reset_password_token");
 ;   DROP INDEX "public"."index_users_on_reset_password_token";
       public         niqjazdxhjbefa    false    199               �   x�}�1�0@��9E.��v��d��,)5����
n��P�/�Op�ǜ�ZV)u�8�k�EJҔ�ۜ���H�Aװ����l�{��FRYj*=L�d}�0���`]���c�6 $�����F)�a�3O         A   x�K�+�,���M�+�,(�O)M.����4204�50�52Q0��25�20�34�033�#����� �U         �  x��W�n7}��b����Vlˊ�Fv\��ڢ�_(rVˊKnH�6ۧ|H��˗tȕT�Iѧ-``I����3�p�zvWk��g��nn�y�%2:/�L�KfL�7�/��f�x1p�io�`#q�S�b��O��K=������Q��'h�"��6��0�D�-h-	"���06�E�`N>�ikf�i̌��%UX��"zҵb���\5���)��ԫmR%*�5�ؒvS/-�/Wvp��
f��Fz��<=��X2�9ƸT�w`4�aO|�O}��|��!��T��U��<��yk�d�A����t}i�E�{u���o��FI�l��F+Qs<�VP�]��̅�A� W��9�Ѽ�
���xd)����J�XWS�	"�ʬ�c�"]G���%�C����Ϭ��)
�
Flv�	*�3qMU�&�X�zh%U�t���"�J�W=��'�Ht��Q��qn*�6I�E/K_4
�؃p���z�\#|�d�Oyh���r�"�4cZR���6���E.�VmYKZF�l�[��\m�^�5JQL"|���G,:S�/����%5��4E9��{�Ơ�Mf7�QRD�B�6�Ӊ�Z�(9�:EKWRCX��"D��Ỵ-	Κ=P�}cΕ$��Kaѕ́��'Ǝ%a�w)�kv�a���*�5@�����Qv%��W҅�S�44��O��cA,��4 �Y'l,[�wo�h� ��#CfS�dӣv.�nhT�0u�T� �V=y�[
7>v��X�8a�lw�Am��s��!���)J��iz/����k<Q���2fW[�l,Q4ʇ�/D�z�5iNi�:C4�u׈�&�V���穲Th���ė�/�G�ϓ��i�����0cV1���%L;��Q���|_q��k8`UMdWL��Qj7F�,��e.�iMa��q���/�ޛ>�|7��t{���~����I��x4˲��%|7����i'C�0�eWo0(��'Yֶm���.��]vw5K���<�;L���=�.�z�bOW�����ا��~~�>}���Y�.WP�����]��x�>}���P��f�lL�xC�p-�2�3�ߜ���Q�P''��)پ9%�����=*�I�?��|����t����������(���>l0<��'��i:���������Ƶ�         �   x�}��n�0���S�����Ҝ�"�ȑKFMQJR6ޞf܆Tɲd}��d{ʅtߞ���+L�<�1^�T�ض��l�2,�,�v'x�)�i�R��r�#}�m"�	N1��4@(P"|�9yӜ��B�5�5ʂBg��V��Bӯ �X�#���I+|�?��Fn�C'�0V�iW׬,�e;Ɨ��]�B*�F� ~��'�bi         d	  x��Y�n�H]��"��Y�S�[/�'�~$�A�1�Zf$&2i�R����)Җ�EZ����}\���<�Je"{ߵ��]�����i�n����rS��&>�3|"aE&��g\�Iz!�
?�r#��nXB�V#�4^��͟��dn<9RsU9W\��������P��+�VZ���nB#F��B�s��k4�\q�s#��s#*I;�O@�g�i�`[㴷z�ƀ:k�[>8BL�(2>}�?�><��m�ɔ�>/0��iW��NK���1�r����4x���~[6e�c����� Ri���>;��Xi�:w�*���r��	�i�٘��P�vUw�����B$T�V����A+aaלkEx��J�*IR���Ƅ���������1d�'�!����knݜňZrV>�0/3�1����s�\wmq&��ĥ�w��g�W�V
	"�|$������	�i�,Ӹ�w���m�K����Ί�w�Y������+)r�Ņ#FA�A��:���l�jW\��ߪ��B�*A���]iwMr�ʀwB�\AQ�xL�|[u�u�-^��ͮ�Ӑ��Ao�+�s��A�ͩ(I�g��0�2�Ӏm���8o��+���on�n]#��!�7U�HJx��$ ̊�J�X�!�HQ������������mnyUvW�uۿ�Z5��r�5�Y`k��Cw�r"G��Oy�ܬ䢢�
	�j� ���{��z���ծ-.�`#�����O&��4��J��\k��,�T4K�1H�I�7{d�R��G�U�yt`^h��'5��5*�0Rϋ�*�wR' �,�j�q�ޔ}�����
���Q���kU�ͪ�5�&��!���Ή�(�sb����u���oU����l����=e�I ��*�'��@(��rcS��#�M؝�ݮ,>��m�]��A��u!�(��n�G@u��	}�1�k"w���n׮����S���Rڲ	U�A��L,xmD��SS��C��4��7���/U�N�H!��>-W���;�Qg	C �q�LJ�۶��ڔ��R+롧A���Q(#cR{�C�T����^Uݾ�����.�4�H[��ۡZ���zH@�|n��QȎ��o��nW�j��ԡ�_��~�T�]�#��8"%�_#t�-��UH���b�25	�_����k�����g�]$n��>"C7���ifE(A]�{1�35ɬweW�(6�b�꟯�p��P��`�pFͩ����3h���Hmw=���b��Ԥ�0�%�@Ux{H�%n
1CF���A�i�+�}�׏��+�G�{�(�w������9�EK$!�81�P'š�j;����*��pS6Ň�oݺJ�Nj�O!����^�6')�|91�j"�_�L쪻�C��8͂�wx��Rıј�&TP�sm'�M�0\J���ۺ���:���Y�� ����m�}��6�&�P����#�]��49�(4���<�0�|:���`�� K��Ef ��s�N	�>J������SQ����Ĭ�H������1f�߯�ڠ���-(�CI�"C(m�\�7�d��3zĬ��Ŀ�U��o_��\%apXq�5ʯp)���'�;1�G!�����u�\�]ۤD��qA�s��sof��
x�n��Ă�7S�Y�T�-)�Rr�!4|��F�*�c�0���p�~��@?��	a��j6D���;�r8Ġ�:@3�v61��a���KIM��VB��+{�mj���>��.J�0�Ke�r1�%��N@�I�&��
���|Jh���2^i%U.�8�	�#��T��s6��c�ĺ-޽���.^��A�Ar�QS�F�.1P�-?ʪ%�9v$�YoΪ�̝}ڵaI�F	Z��.�U�G�"oЭ��6���� ��P��m���77�	Ji_���c$L�6L��7
�Q�0#�s>�I�����?0I�ۥ4���x��oP^�\��
��=1�i�}�1��ʓ��Q�'��z%f�|$s�!�-T� �1axZ8���A0���b�ݴ��l�5ʤk1hV���ݨ�<lfѫR6�AÝ'ˠ{��EW5��3|���$׆Y��� ���:�nJ�PT��N�/2t^r+���9��M�{��R�����*�s�(�V���P���Ko��9td4!in5B?�7e�~�@���a�>VZob��@G�� �)ew|�I��c�Sn���c�� ���}<3�#ULN�;�sBZ�o䖒���V�6�SB�|\W�fD�Y�^<|I��r��	##�	J)�����p�d�3ڃ J��X��UW}��f�FEu���)�r�b��'l��ԱS�=
��$�<����g�/A�
)���|���:���i�Z��2���s���m=�            x��[ے�6�}.���KtE HV�g�Ֆ��J�vZcGl�E��h��2/�*?�76b�a�e��_�'�uQ�$��b_F�n��$�'O&`9���j�����A4�ÙX��E<�cϗ�������1��\��0�d��
'�����=�6e۰aHV-�y��رBwe�6�Kvޭ���PA豫�Lkצ��o�V��.wl�f�V/������K���Fgy�b�͗��Ԧ�����l[W׹m�Y�mk�1�wm
H�S_�vY�lQ�k�ί1
�e�ҵ��vǖ��ͫ��؏��%��9�ښ] Ok_�5f۪i�Ea�/3]�%S��̰�z7e�N׺l�!��Ӳ�2gWu�[�VV-�J(/3�IIf�]��4{AK_�%$�3� On�L;!��20~�_����]�v�.�G��%���v)������'wf�kc������lm���c?��!����Uݒ�7��+hk�����r�n3̞�,�m�h�~K�ۉJ��5��-�Qu��,�x�0
f�T�(
�����0��?��%Svn�jۙ����R��\��Y���j�US��ZU����t�m��|Yk�NS�_>^����?yrss㭪jU��d����Y��_��/� �_�ģ I�<p���8��#�AP/�\"�x��G"��J� I�䉊�q`�?������y�s|�#?��@xޣ}k�w��$]$ZD<����f���J�e̳G���2�γ�(�첮������4�D��y$��(&��x�<f7y֮�|�����ݷÓU֢�3S���]������/�+�Ͳ+�&��)���'N�z��^��v��S��*��O����_���ڔ:�{�
��s�x*�Er����KB�(���OV}of���z�#���+��#/5����z�q�����ӲC˧^�tWn=&V�A-�S�y^���X�,��MU ��S©�z~RI�ڬ��c��W�2k�Hw�+vN��` ]�}r�O1pZtvƆ��O=��u����I�UU��aV/c/� � ��O���c{�J��A���.�_��?�X������=f�n5>�X��O1�{b�į���8�Ax2Va�s!	 ��0U
I���H4
�*� ��G�0ѷ ����@/�TK%�`��3�����������q_~.H<��+��j2��1���xSw�Zt�����~�k�S��
�H���p�C�_4��`6]`4�� �'�� }�d�DLVc��D�1��Za����uO���g?w���#��w�1�{Z�d��z�D=A��!��4��������R;kw[��K����i���d����M��9����I���.�<�H��<�ҾkM�P�쀪���u��K��d0���ǡ����ò��U�܂&�a��K续y�O�������ؙ/����Zc��R�!�i�-���ӆ|�[Ap��:�F:�8v��C��R�h���6g��>QBl�����t��2��sl�f���J�i����d�I*S�Հ���ԀȪ�6;�hJ��Xo��ң-��>�q�D�Ox���I��a(9�G̒��f`�8��X����P�@� &{�A��x$�L|/��U@؈�I��`����2.���H��;��ٸ��+��� (���"?(&�=��(����I!�������2Ša��Ɠ+�c/�4?��P̑�
 i���0A�����F�v��;�㹱�Z�&%���v�a#ʕ���v8��w+=&o�v)<�!%����ZVv���vP�@8zF�>g;͙�#/%jssL��UC��رU	K�������p��q�����Q~�@M�	*����F�U]�eqǙn�H���yP}(�D�����8$�_��8�EP�S>�1��uS�/y�fp�4
Җ���щ�Iʑ�Ƶ�n��#��\`��M���sӽCԥ��`m�z�!���#=C�C��D�d���߬���;����G��}���B,��"{
|<T����v�Y�\�G w2>Y3rG�:A�a�yk���U�O�}`��T�|YW�Ch=�j�E^
���e�/\�,W����(������6�ȳfQ͈�>��O�_�)(���
	;G,����ze����!Y c/���~���������+B��B��C���%R��V�ʲ(3�]��8{��ѱ���_Ȑ��Z�V�Ӈ y���X��OEcmN��"�¦��Ճ�e'�d�C/ B�S>���|�� ��C��<Q�l��R��|�'����� �Of��h�cE����AB��3�ěj�W�b�6/���|��>}�=�[uy�H�-��Ku��c�f�׶��/ c�o�iZu%R����
嗫��9`,��EAڠ�eu��V�4uU�eo�m��`�w�R��������
 ٸ g3B Af
���9]z� ѳ�������5�S�ٗ?\�Zn۲ڤ�5���bp��0R	�{�m_���0�i�6�#m,�:ɪ4��j�]��lWB�[��1���Kߔ9$G_ˬO��Ȱ��XE�C�-
Ħqc�N�QxZ�P��ط�-���5�����*phP�9Ӧn���ȭZW/�Μ���2v.Z�4�ϱ�X�3����H�!�d�:�uZ�Śnq��9��TH�H�98�tu��2���� ����1�(B�B$�
�܇��UKn�X�e���z��۹��?����_>�a>[p���@tI�`#���=q����ИB���=�#��S]/tV5Gg��<H���<�a0G��#��d1t?_��;Zc=�Y�xv3���`f`t?Tu~�m��S����IX�o�D�J���M��k�7�ya�� Ӯ�m��99j�җ�J��Voz�<��A�(192�XA��h��'�-�ȶ7�{()��NO���Q_�k����<�*��Pz��
D�&oZ,scL{P����Ɔ�r�m��J7-a�Fkf�I���R�}��j�j�����P����*B~ Rn�mE�Rڣ�wT,�C���6�X�/s��e���5�`�.@�4�KNHN}���ö�=��XI9f�Կ�3�Zl�D%TZ�[ͱȸ]�0#�d��Ӭ��\aH;k2�"���(B���N�.LM7^��4�9v�$'" ��Xm/��y�$���J�7�R�㙟�E�ÇK**f��3��V�pk%�|�뷠�5F�����SD~��1�T�za����Pa%T�(M��ē���Ț n�@�X@1��8�ouC,e��(I��qՍ����C�@� �.��8{�o��ه���(��	ùͿmT!�t��� �R�DI0�2�=�SF�'�࡫N��)�\"!��_8��!�K�! KP	�w>��~<�D�Pbu(�j����H�?�g���GX��|k�������:���8������lO���s ��egfPi��E��r��*%�,�A����c���C����-ܒ��q�2DL�Ժ�5�~_���li���F��~���V����_��~X
=G����[:�d��Y���!�h*,2s�����w�ކ>�p���:�;��ݨ��upx�ѴN��h~2�.����BKmE?�,��FM�5B��Œh�j�Mv�yT����i���i�|�8_�hKۿ�y�!h��e�C`���\�u'�6b�/݄��K�A�xp��=�`Os������Ƽ��W�N�w����-J��4x!/��'
{�%<
!�H�[��� l>�Ɯ+��{Bw�)#�G���� @� U�Ac�����3��]\��ۿ���i!Q��v%�A�q�١����		��_���W�o�F�N��D2�TJ�⼘�E܇�'?��������+�{�E�j�n�ʐ=<{�j�I-��锝8<���^���ӧ���I��ΙO�(|E@R��t}c�k�=����wl�0���9,e��J�c^�6tٍ�y�j���0M!�dE��V;5?�j��vh�H�/��V�[ǯa�M��h��T�ta�o�D}Q˂)ݩ0% �  �n�����'� ��Y���gT�r�;
�bmm����{6�nc�e|��Gb0��W��ub[�9�M|w io�>~���|���B�>���[�U���YT���z����Xv� �%\�b���÷�ꖔK��{������]iz˿�ρ S� �%JB/��L!yQ6�Dʋ� ��jo�S��q% ڀ�@�+m��ܛ-U�X�$��ƕ(NR�x*u����N��3���w�a(�.�sE��m��`��pD�y�3�����1y^Tu>��g\����$@�~�W���q�(��d��&�������k$�?�� �E�~2VY��0Xh�o��g���+Nh���wWmQ���%q�!p�zp��
	��j�����{��_&;Ė��zKb3k�B+����"V���r��B���n,��}yH�3����n��y� T7zg�/��[��_�X�u^W�h�fmܑ���۵;k8��X���HwgRĳ$��C��	yȞm�0;��WN��_�z����� �ơ�0���r:"rľ�a�B�X�������/�"B/q�brg�V<���2tȑqhR*��@��w^�w�B�� ��k�g�����4%��^�h�TJ�T��a�xpm�צ�^�eC;�ŏ�_|�qO9         j  x��X�r�F}|�<n����_��(Q���rm����c6� б�5������H�^\~�>��s�4��hˆ^7UY/�ؑ�_�|ݗ�#�^��޷�b=�˦�?u�["��i+gJHA㗞Q)�����ܐU���;�{j��]̋>�e�ˊH���p3n�����sɤ����L���شŒ�)�~Yŗ�wmQ�ˎ�c]Te�B^�>�:�-���c�����T���C-�x���w?l<��.��������4s:���].3�/3M��u�w��S���j�*���bH�U�Ꮲ�'�S��G|�>n>=�K���\+��.\0e���E�ڠ},kr���q�)��Cŵ�b�3��"��s�H����G �ύb�q�'�B.s(���!�E[�]9_�kG���J�~�uO��a����3|����L*�\�y��ڭ��0��t)O����=6e�\�E�������2�n���mj���BG��[��3 ��F���-��L�#�벪b�j�m�tݸ̔'PN;s�(�/��y���k���>�N�F�k�qs�B�pޒyrU�˦��-V�X�I��l����f��82�r%s�*$��`ʄ"7E]"��|�{,�"�~��Y0����t�ơ�μ13�qI7N���I�v��GtB^0e��fUt��5�x�cdkf���LB؜&���%�<�m|FP�z�Da�:uۗX�{�L��`>�*��N�;qޒi��?K���Q>ʲ#v7���4�r��c�)S�������?���"7>W�qm�d�pȨ�.�� `�6�/'�@��fƵ���RY�3Yr�����	�=�03E.uJRPV��u��W5�-[4�@�ݧ�c Oa0����{g��;]��c�(y��#8Glt����0s��)8*���9��ˮI��q7�
�"���WƇ��*�}� ?�Q�w�~�P����Nz㜼`�0��5����G��q�m�����h9���fbʔ!�F\�o��'���f�]�lD�S�Z�/�2i�]Q�b�'0;2�$�b�rk^C��0ɱS,z4f&�&̬�zh1��9�������\�5Reƈlj{���>C���F#�5�O���	�uS�m�&.q�� �|�L#~�	�r���a���(�42�w�B���8m��(Ϳ�ι�	(��]�x93��0v�|g�:%������6ʤK�_WU3_N�@ }C_�&i��N�&�G³wJ�I��@��ԧ9]�	��"6����n��i�j�o�:��!F��`���O�c{&��e2��M�La�o�EJ�(c2sÂUfZG��[m��P����'٪�4���W���`6d��N�)qf)F�fx�n�v��5�@	/��� �/��"�f7����>v$��L[���`ʄ�4�#��W\�-)��#���iP9jP��60��~ۚ�9�dڜIYa2����;:�Y��,�]�@vq-�LB�|b��4����V�OR2-���S����UYA#@)�%��=�A<D{���(7�7z?bO�2m�U?B�aѻ/�UA��=C���h�ՋMԉ��`��Jw��Ai�c[.��n��SUt�z4�,Q�� �����k{4Ki���$^��0���Ǣ_�/5���>ݑ7뮬c��ȃ�y^�E��Xc�i]b�_�uԱ)S�B���jA��I����ï X���`�T��)pQ�k����T�R��A��$��Z_2eڒ>Cl��ו��!�w�8� �� =.�K���N�IK�2�^�� �������_��@�;o���tɔiO���<]���}B���aǎLj5��r�)3���6�EAN��+	�'�Qb��6�MԆ�kf�L�3Z�_n����i�Ul����{����%�Y.��'b�4�\*�o,˲?%�"q          W   x�Uλ�0C���p$ǿ���s�"�P��JO����F����#�΂6��G�<�͉�z9h4��\�}I=������"�3��I����U%B      !   �   x�m��
�@E��W�H��-�9��M4j�B�(��_o�B�4�����U���r����Z��'�2B������ne=;f���V�Ǻz�N]s��95���<�b�v��Iuo��X����Ō!Z�k#Z����1�[��WJ:�4�W��[K`vV-YI�Zj�;>1�
	<44w�����C�$��BA�Zg=     