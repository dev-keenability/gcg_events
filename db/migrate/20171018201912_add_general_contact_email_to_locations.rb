class AddGeneralContactEmailToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :general_contact_email, :string
  end
end
