class AddTitleToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :title_for_slug, :string
    add_index(:locations, [:title_for_slug], {:unique => true})
  end

end
