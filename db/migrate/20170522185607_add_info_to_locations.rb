class AddInfoToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :description, :text
    add_column :locations, :phone, :string
    add_column :locations, :fax, :string
    add_column :locations, :specific_location, :string
  end
end
