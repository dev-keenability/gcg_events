class CreateGalleryImages < ActiveRecord::Migration[5.0]
  def change
    create_table :gallery_images do |t|
      t.float :position
      t.attachment :pic
      t.timestamps
    end
  end
end
