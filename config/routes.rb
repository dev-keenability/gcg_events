Rails.application.routes.draw do
  require 'admin_constraint'
  mount Ckeditor::Engine => '/ckeditor', constraints: AdminConstraint.new
  root 'home#index'
  resources :profiles, except: [:index]

  match '/who-we-are'              =>        'home#who_we_are',              via: [:get],            :as => 'who_we_are'
  # match '/gallery'              =>        'home#gallery',              via: [:get],            :as => 'gallery'
 
  match '/join-our-team'              =>        'home#join_our_team',              via: [:get],            :as => 'join_our_team'
  # match '/clients'              =>        'home#clients',              via: [:get],            :as => 'clients'
  # match '/affiliations'              =>        'home#affiliations',              via: [:get],            :as => 'affiliations'
  match '/privacy-policy'              =>        'home#privacy_policy',              via: [:get],            :as => 'privacy_policy'


  match '/what-we-do'                 =>        'what_we_do#what_we_do',              via: [:get],            :as => 'what_we_do'
  match '/social-events'              =>        'what_we_do#social_events',              via: [:get],            :as => 'social_events'
  match '/weddings'              =>        'what_we_do#weddings',              via: [:get],            :as => 'weddings'
  match '/corporate-events-catering'              =>        'what_we_do#corporate_events',              via: [:get],            :as => 'corporate_events'
  match '/sporting-events-catering'              =>        'what_we_do#sporting_events',              via: [:get],            :as => 'sporting_events'
  match '/government-events-catering'              =>        'what_we_do#government_events',              via: [:get],            :as => 'government_events'

  match '/gallery'              =>        'gallery_images#index',              via: [:get],            :as => 'gallery'
  match '/gallery/new'              =>        'gallery_images#new',              via: [:get],            :as => 'new_gallery'
  match '/gallery'              =>        'gallery_images#create',              via: [:post]
  match '/gallery/:id'              =>        'gallery_images#destroy',              via: [:delete], :as => 'delete_gallery'

  devise_scope :user do
    get "/users/sign_up",  :to => "devise/sessions#new"
  end  
  
  devise_for :users

  devise_scope :user do 
    get "/admin" => "devise/sessions#new" 
  end

  #########################################
  #==locations url for location model
  #########################################
    # get '/locations', to: 'locations#index', as: :locations
    post 'locations', to: 'locations#create'
    get '/locations/new', to: 'locations#new', as: :new_location
    get 'locations/:title_for_slug/edit', to: 'locations#edit', as: 'edit_location'
    get 'locations/:title_for_slug', to: 'locations#show', as: 'location'
    patch 'locations/:title_for_slug', to: 'locations#update'
    put 'locations/:title_for_slug', to: 'locations#update'
    delete 'locations/:title_for_slug', to: 'locations#destroy'

  #########################################
  #==news url for blog model
  #########################################
    get '/news', to: 'blogs#index', as: :blogs
    post 'news', to: 'blogs#create'
    get '/news/new', to: 'blogs#new', as: :new_blog
    get 'news/:title_for_slug/edit', to: 'blogs#edit', as: 'edit_blog'
    get 'news/:title_for_slug', to: 'blogs#show', as: 'blog'
    patch 'news/:title_for_slug', to: 'blogs#update'
    put 'news/:title_for_slug', to: 'blogs#update'
    delete 'news/:title_for_slug', to: 'blogs#destroy'
    
  match "/contacts"              =>        "contacts#create",              via: [:post]
  match "/applicants"            =>        "applicants#create",            via: [:post]

  get '/sitemap.xml', to: redirect("https://s3-us-west-2.amazonaws.com/gcg-events/sitemaps/sitemap.xml.gz", status: 301)

  match "*path", to: "home#catch_all", via: :all

end
