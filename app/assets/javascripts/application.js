// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require plugins/jquery.readySelector
//= require bootstrap
//= require light-gallery
//= require owl.carousel.min
//= require ckeditor/init
//= require turbolinks
//= require_tree .

$(document).on('turbolinks:load', function() {


  // Select all links with hashes
  $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .not('[href="#carousel-example-generic"]')
    .click(function(event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
        && 
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 2000, function() {
            // // THIS SECTION ADDS A FOCUS TO THE AREA SCROLLED - ALEX
            // // Callback after animation
            // // Must change focus!
            // var $target = $(target);
            // $target.focus();
            // if ($target.is(":focus")) { // Checking if the target was focused
            //   return false;
            // } else {
            //   $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            //   $target.focus(); // Set focus again
            // };
          });
        }
      }
    });



    $('.ckeditor').each(function() {
      CKEDITOR.replace($(this).attr('id'));
    });

  	$('.owl-carousel').owlCarousel({
        loop: true,
        autoplay: 2500,
        margin: 10,
        responsiveClass: true,
        responsive: {
          0: {
            items: 1,
            nav: true
          },
          620: {
            items: 2,
            nav: true
          }, 
          992: {
            items: 3,
            nav: true,
            loop: true,
            margin: 20
          }
        }
    })

    // I have 3 different views using the following functions, therefore you will see the id's separated with commas
    if ($('body.blogs.edit').length > 0 || $('body.blogs.new').length > 0 || $('body.profiles.new').length > 0 || $('body.profiles.edit').length > 0 || $('body.locations.new').length > 0 || $('body.locations.edit').length > 0) {
      $("#blog_images_attributes_0_pic,#profile_image_attributes_pic,#location_image_attributes_pic").change(function(evt) { 
        if (evt.target.files && evt.target.files[0]) {
            var reader = new FileReader();

            reader.onload = function (event) {
                $('#current_image')
                    .attr('src', event.target.result)
                    .width(300)
                    .height(169);

                $('#square_image')
                    .attr('src', event.target.result)
                    .width(300)
                    .height(300);

                $('#location_image')
                    .attr('src', event.target.result)
                    .width(500)
                    .height(250);
            };

            reader.readAsDataURL(evt.target.files[0]);
        }
      });
    } 


    // Initialize google maps javascript api
    if ($('body.home.index').length > 0 || $('body.home.who_we_are').length > 0 || $('body.home.catch_all').length > 0) {
        var initMap = (function() {
          
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 3,
                scrollwheel: false,
                center: new google.maps.LatLng(19.466766,-70.2060925)
                // mapTypeId: 'terrain'
            });

            var infowindow;   

            // This is how we get the locations as a javascript object
            var locations = $('#myid').data('mylocations');
            // console.log(locations[0].name);
            // console.log(locations[0].latitude);
            // console.log(locations[0].longitude);

            function addMarker(location) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(location.latitude,location.longitude),
                    icon: 'https://s3-us-west-2.amazonaws.com/gcg-events/general/gcg_events_icon.png',
                    optimized: false,
                    // zIndex:Date.now(), //so that last markers in the loop overlay the earlier ones
                    map: map
                    // title: feature.title
                }); 
                // We are declaring the content attribute to the marker so that we can reference it later with on click (this)
                marker.content = "<p>" + "<a style='padding-right:7px;' href='/locations/" + location.title_for_slug + "'>"  + location.name + "</a>" + "</p>"
                infowindow = new google.maps.InfoWindow(); 
                google.maps.event.addListener(marker, 'click', function() {
                    if(!marker.open){
                        infowindow.setContent(this.content);
                        infowindow.open(map,marker);
                        marker.open = true;
                    }
                    else{
                        infowindow.close();
                        marker.open = false;
                    }
                    google.maps.event.addListener(map, 'click', function() {
                        infowindow.close();
                        marker.open = false;
                    });
                });
            }


            for (var i = 0, location; location = locations[i]; i++) {
              addMarker(location);
            }

        }());

        // initMap(); // Call initMap here to avoid initMap is not a function error, i decided to get fancy and call it with () at the end of the function.
    } // home.index


    // This is to remove scroll zoom on iframe
    if ($('body.locations.show').length > 0) {
      $('.map-container').click(function(){
        $(this).find('iframe').addClass('clicked')
      })  
      
      $('.map-container').mouseleave(function(){
        $(this).find('iframe').removeClass('clicked')
      });
    }
    
});