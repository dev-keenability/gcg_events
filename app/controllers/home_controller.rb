class HomeController < ApplicationController
  def index
  end

  def about
  end

  def who_we_are
  end

  # def gallery
  #   @gallery_images = GalleryImage.all
  # end

  def join_our_team
  end

  def clients
  end

  def affiliations
  end
  
  def privacy_policy
  end

  def catch_all
    flash.now[:warning] = "The page or request you were looking for was not found."
    render :index
  end

end
