class WhatWeDoController < ApplicationController

  def what_we_do
  end

  def what_we_do_social_events
  end

  def what_we_do_weddings
  end

  def what_we_do_corporate_events
  end

  def what_we_do_sporting_events
  end

  def what_we_do_government_events
  end
end