class GalleryImagesController < ApplicationController
  # before_action :set_blog, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index]
  def index
    @gallery_images = GalleryImage.order(position: :asc)
  end

  def new
    @gallery_image = GalleryImage.new
  end

  def create
    @gallery_image = GalleryImage.new(gallery_image_params)
    if @gallery_image.save
      redirect_to gallery_path, notice: 'Gallery image was successfully created.'
    else
      render :new
    end
  end

  def destroy
    @gallery_image = GalleryImage.find(params[:id])
    @gallery_image.destroy
    redirect_to gallery_url, notice: 'Gallery image was successfully destroyed.'
  end

  private

    def gallery_image_params
      params.require(:gallery_image).permit(:position, :pic)
    end

end
