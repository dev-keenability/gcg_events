class Profile < ApplicationRecord
  belongs_to :location
  has_one :image, as: :imageable, :class_name => "Profile::Asset", dependent: :destroy
  accepts_nested_attributes_for :image, update_only: true #otherwise it will delete and then reinsert everytime you reach the edit page, instead of updating it
  # accepts_nested_attributes_for :image, allow_destroy: true # use above for has one relationship
end
