class GalleryImage < ApplicationRecord

  Paperclip.interpolates :id do |attachment, style|
    attachment.instance.id
  end

  has_attached_file :pic,
    :path => "#{Rails.env}/gallery_image/:id/:style/:filename",
    :styles => { 
      :large => ["900x600>", :jpg]
    },
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

  validates_attachment :pic, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true
end
